#!/usr/bin/env ruby

require 'json'

queue_name = ARGV[0] || "defaut-queue-name"
max_count = 600

command = `curl -s -u username:password http://localhost:15672/api/queues/vhost/#{queue_name}`

decoded =
begin
  JSON.parse(command)
rescue => e
  exit 1
end

message_count = decoded["messages"]

if message_count > max_count
  puts "#{queue_name} has #{message_count} messages in the queue"
  exit 2
else
  puts "#{queue_name} message count OK"
  exit 0
end